import Dao.*;

import java.util.*;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] a) {

        init();
        AirportDao.printFlights(AirportDao.getFlights());
        AirportDao.printPassengers(AirportDao.getPassengers());
        AirportDao.printTickets(AirportDao.getTickets());
        System.out.println("------------join JPQL");
        AirportDao.printJoin(AirportDao.joinBYJPQL());
        System.out.println("------------join NamedQuery");
        AirportDao.printJoin(AirportDao.joinByNamedQuery());
        System.out.println("------------join Native Query");
        AirportDao.printJoin(AirportDao.joinByNativeQuery());
        System.out.println("------------join Criteria");
        AirportDao.printJoin(AirportDao.joinByCriteria());
        System.out.println("------------order JPQL");
        AirportDao.printOrder(AirportDao.orderByJPQl());
        System.out.println("------------order NamedQuery");
        AirportDao.printOrder(AirportDao.orderByNamedQuery());
        System.out.println("------------order Native Query");
        AirportDao.printOrder(AirportDao.orderByNativeQuery());
        System.out.println("------------order Criteria");
        AirportDao.printOrder(AirportDao.orderByCriteria());
        System.out.println("------------ManyToMany");
        AirportDao.printManyToMany(AirportDao.checkManyToMany());

    }

    static void init() {
        Passenger passenger1 = new Passenger(1, "John");
        Passenger passenger2 = new Passenger(2, "Andrew");
        Passenger passenger3 = new Passenger(3, "Michael");
        Passenger passenger5 = new Passenger(4, "DIma");
        Stream.of(passenger1,passenger2,passenger3,passenger5).forEach(AirportDao::insertEntity);
        Ticket ticket1 = new Ticket(1, 1, 1);
        Ticket ticket2 = new Ticket(2, 2, 1);
        Ticket ticket3 = new Ticket(3, 3, 1);
        Ticket ticket4 = new Ticket(4, 4, 2);
        Stream.of(ticket1,ticket2,ticket3,ticket4).forEach(AirportDao::insertEntity);
        Flight flight1 = new Flight(1, "Samara", "Moscow");
        Flight flight2 = new Flight(2, "Samara", "Saratov");
        Stream.of(flight1, flight2).forEach(AirportDao::insertEntity);

        passenger1.setTickets(getListFromEntity(ticket1));
        passenger2.setTickets(getListFromEntity(ticket2));
        passenger3.setTickets(getListFromEntity(ticket3));
        passenger5.setTickets(getListFromEntity(ticket4));
        passenger1.setFlights(getSetFromEntity(flight1,flight2));
        passenger2.setFlights(getSetFromEntity(flight1));
        passenger3.setFlights(getSetFromEntity(flight1));
        passenger5.setFlights(getSetFromEntity(flight2));
        Stream.of(passenger1,passenger2,passenger3,passenger5).forEach(AirportDao::mergeEntity);
        ticket1.setPassenger(passenger1);
        ticket1.setFlight(flight1);
        ticket2.setPassenger(passenger2);
        ticket2.setFlight(flight1);
        ticket3.setPassenger(passenger3);
        ticket3.setFlight(flight1);
        ticket4.setPassenger(passenger5);
        ticket4.setFlight(flight2);
        Stream.of(ticket1,ticket2,ticket3,ticket4).forEach(AirportDao::mergeEntity);


//        Stream.of(ticket1,ticket2,ticket3).forEach(ticketEntity -> ticketEntity.setFlight_entity(flight1));
//        ticket4.setFlight_entity(flight2);
//        ticket1.setPassenger(passenger1);
//        ticket2.setPassenger(passenger2);
//        ticket3.setPassenger(passenger3);
//        passenger1.setTickets(getListFromEntity(ticket1));
//        passenger2.setTickets(getListFromEntity(ticket2));
//        passenger3.setTickets(getListFromEntity(ticket3));
//        passenger4.setTickets(getListFromEntity(ticket4));

    }

    static <T> List<T> getListFromEntity(T entity) {
        ArrayList<T> arrayList = new ArrayList<>();
        arrayList.add(entity);
        return arrayList;
    }
    static <T> Set<T> getSetFromEntity(T ... entity){
        HashSet<T> set=new HashSet<>();
        Arrays.stream(entity).forEach(set::add);
        return set;
    }

}
