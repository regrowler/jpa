package Dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.stream.Collectors;

public class AirportDao {
    static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("com.ikosmov.jpa.hibernate");
    public static final String sqlJoin = "select t.id,f.startPoint,f.targetPoint,p.name from Flight f,Passenger p,Ticket t " +
            "where t.passengerId=p.id and t.flightId=f.id";
    public static final String sqlOrderBy = "select p.name from Passenger p order by p.id desc";

    public static void insertEntity(AirportEntity entity) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.persist(entity);
        em.getTransaction().commit();
        em.close();

    }

    public static void mergeEntity(AirportEntity entity) {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        em.merge(entity);
        em.getTransaction().commit();
        em.close();

    }

    public static void printTickets(List<Ticket> list) {
        list.forEach(ticket -> {
            System.out.println(String.format("Ticket with id %s on flight %s owned by passenger with id %s",
                    ticket.getId(),
                    ticket.getFlightId(),
                    ticket.getPassengerId()));
        });

    }

    public static void printPassengers(List<Passenger> list) {
        list.forEach(passenger -> {
            System.out.println(String.format("Passenger with id %s named %s",
                    passenger.getId(),
                    passenger.getName()));
        });
    }

    public static void printFlights(List<Flight> list) {
        list.forEach(flight -> {
            System.out.println(String.format("Flight with id %s starts at %s and ends at %s",
                    flight.getId(),
                    flight.getstartPoint(),
                    flight.gettargetPoint()));
        });
    }

    public static void printJoin(List<Object[]> list) {
        list.forEach(objects -> {
            System.out.println(String.format("Ticket with id %s from %s to %s owned by %s",
                    objects[0],
                    objects[1],
                    objects[2],
                    objects[3]));
        });
    }

    public static void printOrder(List<Object> list) {
        list.forEach(System.out::println);
    }

    public static void printManyToMany(List<String> list) {
        list.forEach(System.out::println);
    }

    public static List<Ticket> getTickets() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List list = em.createQuery("from  Ticket ", Ticket.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Passenger> getPassengers() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List list = em.createQuery("from Passenger ", Passenger.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Flight> getFlights() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List list = em.createQuery("from Flight ", Flight.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object[]> joinBYJPQL() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object[]> list = em.createQuery(sqlJoin).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object[]> joinByNamedQuery() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object[]> list = em.createNamedQuery("TicketEntity.joinAll").getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object[]> joinByNativeQuery() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object[]> list = em.createNativeQuery(sqlJoin).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<String> checkManyToMany() {
        EntityManager em = entityManagerFactory.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Passenger> query = builder.createQuery(Passenger.class);
        Root<Passenger> entityRoot = query.from(Passenger.class);
        List<String> res = em.createQuery(query)
                .getResultList()
                .stream()
                .map(passenger -> {
                    StringBuilder builder1 =new StringBuilder();
                    builder1.append("Passenger ")
                            .append(passenger.getName())
                            .append(" has flights:");
                    passenger.getFlights().forEach(flight -> {
                        builder1.append("\n");
                        builder1.append(" from ").append(flight.startPoint);
                        builder1.append(" to ").append(flight.targetPoint);
                        builder1.append(" with id ").append(flight.getId());
                    });
                    return builder1.toString();
                })
                .collect(Collectors.toList());
        em.close();
        return res;
    }

    public static List<Object[]> joinByCriteria() {
        EntityManager em = entityManagerFactory.createEntityManager();
        CriteriaQuery<Ticket> query = em.getCriteriaBuilder().createQuery(Ticket.class);
        Root<Ticket> entityRoot = query.from(Ticket.class);
        query.select(entityRoot);
        List<Object[]> res =
                em.createQuery(query)
                        .getResultList()
                        .stream()
                        .map(ticket -> {
                            Object[] re = new Object[4];
                            re[0] = ticket.getId();
                            re[1] = ticket.getFlight().startPoint;
                            re[2] = ticket.getFlight().targetPoint;
                            re[3] = ticket.getPassenger().getName();
                            return re;
                        })
                        .collect(Collectors.toList());
        em.close();
        return res;
    }

    public static List<Object> orderByJPQl() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object> list = em.createQuery(sqlOrderBy).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object> orderByNamedQuery() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object> list = em.createNamedQuery("PassengerEntity.orderBy").getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object> orderByNativeQuery() {
        EntityManager em = entityManagerFactory.createEntityManager();
        em.getTransaction().begin();
        List<Object> list = em.createNativeQuery(sqlOrderBy).getResultList();
        em.getTransaction().commit();
        em.close();
        return list;
    }

    public static List<Object> orderByCriteria() {
        EntityManager em = entityManagerFactory.createEntityManager();
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Passenger> query = builder.createQuery(Passenger.class);
        Root<Passenger> entityRoot = query.from(Passenger.class);
        query.select(entityRoot).orderBy(builder.desc(entityRoot.get("id")));
        List<Object> res = em.createQuery(query)
                .getResultList()
                .stream()
                .map(passenger -> passenger.getName())
                .collect(Collectors.toList());
        em.close();
        return res;
    }

}
