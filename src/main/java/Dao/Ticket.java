package Dao;

import javax.persistence.*;

@Entity
@NamedQuery(name = "TicketEntity.joinAll", query = "select t.id,f.startPoint,f.targetPoint,p.name from Flight f,Passenger p,Ticket t where t.passengerId=p.id and t.flightId=f.id")
public class Ticket implements AirportEntity {
    public Ticket(int id, int passengerId, int flightId) {
        this.id = id;
        this.passengerId = passengerId;
        this.flightId = flightId;
    }

    @Id
    private int id;
    public int passengerId;
    private int flightId;
    @ManyToOne
    private Passenger passenger;
    @ManyToOne
    private Flight flight;

    public Ticket() {
    }

    public int getId() {
        return id;
    }

    public void setId(int ticket_id) {
        this.id = ticket_id;
    }

    public int getFlightId() {
        return flightId;
    }

    public void setFlightId(int flight_id) {
        this.flightId = flight_id;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(int passenger_id) {
        this.passengerId = passenger_id;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight flight) {
        this.flight = flight;
    }
}
