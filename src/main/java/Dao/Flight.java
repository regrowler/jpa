package Dao;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Flight implements AirportEntity {
    public Flight(int id, String startPoint, String targetPoint) {
        this.id = id;
        this.startPoint = startPoint;
        this.targetPoint = targetPoint;
    }

    @Id
    private int id;
    public String startPoint;
    public String targetPoint;

    @OneToMany(mappedBy = "flight")
    private List<Ticket> flight_entities;
    @ManyToMany
    private Set<Passenger> passengers;

    public Flight() {
    }

    public int getId() {
        return id;
    }

    public void setId(int flight_id) {
        this.id = flight_id;
    }

    public String getstartPoint() {
        return startPoint;
    }

    public void setstartPoint(String startPoint) {
        this.startPoint = startPoint;
    }

    public String gettargetPoint() {
        return targetPoint;
    }

    public void settargetPoint(String targetPoint) {
        this.targetPoint = targetPoint;
    }

    public List<Ticket> getFlight_entities() {
        return flight_entities;
    }

    public void setFlight_entities(List<Ticket> flight_entities) {
        this.flight_entities = flight_entities;
    }
}
